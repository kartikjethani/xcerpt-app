import React from 'react';
import {useState, useEffect, FC} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistCache} from 'apollo3-cache-persist';
import {setContext} from '@apollo/client/link/context';
import {
  ApolloProvider,
  ApolloClient,
  InMemoryCache,
  HttpLink,
  from,
} from '@apollo/client';
import {onError} from '@apollo/client/link/error';
// import {relayStylePagination} from '@apollo/client/utilities';
import {AuthToken} from '../../types/auth/auth-token.type';
import {globalConfig} from '../../config';
import {AppLogo} from '../../views/screens/splash/app-logo';

const {baseApiUrl} = globalConfig;

const errorLink = onError(
  ({graphQLErrors, networkError, operation, forward}) => {
    if (graphQLErrors) {
      graphQLErrors.forEach(async ({message, locations, path}) => {
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
            locations,
          )}, Path: ${JSON.stringify(path)}`,
        );

        // console.log(extensions?.code);

        switch (message) {
          // Apollo Server sets code to UNAUTHENTICATED
          // when an AuthenticationError is thrown in a resolver
          case 'Unauthorized':
            // Modify the operation context with a new token
            const oldHeaders = operation.getContext().headers;
            operation.setContext({
              headers: {
                ...oldHeaders,
                authorization: await getAuthToken(),
              },
            });
            // Retry the request, returning the new observable
            return forward(operation);
        }
        return;
      });
    }
    if (networkError) {
      console.log(`[Network error]: ${networkError}`);
    }
  },
);

const getAuthToken = async () => {
  const token = await AsyncStorage.getItem('@auth_token');
  if (token) {
    const jsonToken: AuthToken = JSON.parse(token);
    const data = {userName: 'guest', refreshToken: jsonToken.refreshToken};
    console.log('getAuthToken------------------------------------');
    const rawResponse = await fetch(`${API_ENDPOINT}/auth/refresh-token`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    const content = await rawResponse.json();
    const {
      idToken: {jwtToken},
    } = content;
    jsonToken.idToken = jwtToken;
    const newToken = JSON.stringify(jsonToken);
    AsyncStorage.setItem('@auth_token', newToken);
    return jwtToken;
  }
};

const cache = new InMemoryCache({
  // typePolicies: {
  //   Query: {
  //     fields: {
  //       feed: relayStylePagination(['type']),
  //     },
  //   },
  // },
});

const authLink = setContext(async (_, {headers}) => {
  // get the authentication token from local storage if it exists
  const token = await AsyncStorage.getItem('@auth_token');

  if (!token) {
    // return guest user token
    const guestAuthToken: AuthToken = {
      userName: 'guest',
      idToken: '', // empty - as it will be fetched via refresh token
      refreshToken:
        'eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiUlNBLU9BRVAifQ.X_ZQjbu-SNQExLQPWccDQ2RutXp3svZ8QPxzhXvc0ER2MfpuI7lTAs-Ud6MgfMt1L5xBb-Lx5vMcN5_fJRGO4c5wWb1JnLNNeVVdAkiTT1AHi8sEMcAA4dnzO9EsgJaLxibqsX9jYw6hBK7CoocLP3G5oYbdoViHKLLoa1ScnG_SuG_r5AqP0kA3zWwO_ze7crQ7MGG02AzrOoorIwAW_LDVeB_upe_79rYcYwJdPKPlKi98gPg-MEb5VxOjzmIEsHOdSwFvxQXYCsHVCtZl36Yt10A6war2h2W2wK-CdjuASvrxUrF5ld_zp1A1F11NVa-d9LK7aEiYTCaclH_P8g.B23QkebGZUMic63U.xi1UXYNGp-6-ojb7vdNvsNW3RWuKnibuJhW38KsBpPjmrtF-CIIw0242Cpadx1czUpeIJkFAFfPQRGugKi6Cp8L2rRubS-gx-s_vk5evI2kNu3PRFWAv5PYPmYoZj_UI-Yw80lRZ845rDuHcpClYNEEg7q18oTsN1RmgM6YDQT4SV9HBjo5vH1kemlX0OaLdexqUv_ZZta9lIMugsWZGKbs0MACt2H6c_tjqHZwDqtgN4yxYSvhX2Fns0jm5mK_CzS97wSY5FK86w0r9IwMWZQxIGDE4h7vP6dVS9sI36rnQMMtWOn5-ib-_neWHMqlEfifqoKKUTRH15_apfy4E3UPGogzmBFeP4FavUWaPemBoBuIZkdX7cQ_yVyiWQpFQzfclgWBB2R-I5CoK5PE_yx7BVwnnhCqfMwTz3OUgX-OPxr4JyyTchoaiSzdwZ8hOpRE93lTBPKCu6BJ8mi1Tvd2xurneFp0T8Jwye_U_lgp-W9VmkJ4LFPbcCFFHPDgdB5GskmWj2dSzLI4DERGHdrbvRg1DSkGyOElDvTSyCGdQFm0eWeN655JKhbRJZ_YIqAcgTn47mC8l4f69Aqmj2WxHwKUyr9ryLX6c_CuY-X_hdIaSG4tV2iQ0tnpEdMlUCxqReBMmRRPoQCrXbCElo8Qtob84isz_OvWTur7yEk8s1Y65sW9Kjd2r6kEVB3ynM8Dx5BATS5sBR8mF4ghjMSOItQJ2au1X56yNHtrKr2i1k0oSugSqUTI_YAZ848_hdAFEbfF5YMDGkch1e195IY9n6WyHZqkScLhgigsCHzN3tI4SrYrJOLgbZ8-SAgqHvPprhMfenJlshcOZcC2URxm-ZqXl0YuJHenHfMCjntTpJCYmnyxNy287hiwlll1MGendCNECbnVzvkjmiP4xqmYTwfPGHWHZfEYIXysrWgqGm-gpb0PwvsvgisrzWhwWjHi6sLxBy4lavMWJSP7YGfBbqQXEC-6JXch_PpjAXREbIj8QcstecWEZaiGXwmcrQLuZnrBCKD8lJTrMK7kKzG1BY4YXjb52j2RgvDJuLceHjEUnY2CCflnQOQlvwYoD_x92MJinUCWX_b0DjeznX25e33l_DUkfDf7RGz78ijilzbk7oyUwghgi8HhnH_0aofvCQ3-46NSqM9x_o9kVt6PulQDvvaZLbFFuH3f057E2mvIrmjWzalieKM7MbnEKbBpU0WADV964srAXuGgGyYf_HynQ6R9TPWqHvBhbVScpaL9DORcmIz0XaYizPZfoRFPOY8syCJg.7oa-d6QpVj9xAVMwE3UFDQ',
    };
    const jsonGuestAuthToken = JSON.stringify(guestAuthToken);
    await AsyncStorage.setItem('@auth_token', jsonGuestAuthToken);
    return {
      headers: {
        ...headers,
        authorization: `Bearer ${guestAuthToken.idToken}`,
      },
    };
  }

  // return logged in user token
  const authToken: AuthToken = JSON.parse(token);
  return {
    headers: {
      ...headers,
      authorization: `Bearer ${authToken.idToken}`,
    },
  };
});

const API_ENDPOINT_DEV = 'http://192.168.0.106:3000';
const API_ENDPOINT_PROD = baseApiUrl;

const API_ENDPOINT =
  process.env.NODE_ENV === 'development' ? API_ENDPOINT_DEV : API_ENDPOINT_PROD;

const httpLink = new HttpLink({
  uri: `${API_ENDPOINT}/graphql`,
});

const client = new ApolloClient({
  link: from([authLink, errorLink, httpLink]),
  cache,
  defaultOptions: {watchQuery: {fetchPolicy: 'cache-and-network'}},
});

const GraphqlProvider: FC = props => {
  const {children} = props;

  const [loadingCache, setLoadingCache] = useState(true);

  useEffect(() => {
    persistCache({
      cache,
      storage: AsyncStorage,
    }).then(() => setLoadingCache(false));
  }, []);

  if (loadingCache) {
    return <AppLogo />;
  }

  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};

export default GraphqlProvider;
