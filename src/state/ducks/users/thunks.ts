import {Dispatch} from 'redux';
import {setLoggedIn, setUserId} from './slices';
// import {createSelector} from 'reselect';
// import {default as useSelector} from './selectors';
// const user = createSelector(state => state.users);

export interface User {
  isLoggedIn?: boolean;
  userId: string;
}

export const setLoggedInThunk =
  ({isLoggedIn}: User) =>
  (dispatch: Dispatch) => {
    dispatch(setLoggedIn(isLoggedIn));
  };

export const setUserIdThunk =
  ({userId}: User) =>
  (dispatch: Dispatch) => {
    dispatch(setUserId(userId));
  };

export default {
  setLoggedInThunk,
};
