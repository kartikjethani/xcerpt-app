import {createSlice} from '@reduxjs/toolkit';

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    isLoggedIn: false,
    userId: null,
  },
  reducers: {
    setLoggedIn: (state, {payload}) => {
      state.isLoggedIn = payload;
    },
    setUserId: (state, {payload}) => {
      state.userId = payload;
    },
  },
});

export const {setLoggedIn, setUserId} = userSlice.actions;

export default userSlice.reducer;
