import {useSelector} from 'react-redux';
import {RootState} from '../../store/store';

export const IsLoggedIn = (): boolean => {
  const user = useSelector((state: RootState) => state.users);
  return user.isLoggedIn;
};

export default {
  IsLoggedIn,
};
