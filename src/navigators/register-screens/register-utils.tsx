import React, {FC, ComponentType} from 'react';
import {Provider, ApolloProvider} from '../../state/providers';
import hoistNonReactStatic from 'hoist-non-react-statics';

export const WrappedComponent = (Component: ComponentType<{}>) => {
  return function inject(props: any) {
    const EnhancedComponent: FC = () => (
      <Provider>
        <ApolloProvider>
          <Component {...props} />
        </ApolloProvider>
      </Provider>
    );
    hoistNonReactStatic(EnhancedComponent, Component);
    return <EnhancedComponent />;
  };
};
