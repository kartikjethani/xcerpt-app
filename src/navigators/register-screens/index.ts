import {FC} from 'react';
import {Navigation} from 'react-native-navigation';
import {SignInScreen} from '../../views/screens/auth';
import {WrappedComponent} from './register-utils';
import {gestureHandlerRootHOC} from 'react-native-gesture-handler';
import CodePush from 'react-native-code-push';
import {SCREENS, HomeScreen, ProfileScreen} from '../../views/screens';
import {SplashScreen} from '../../views/screens/splash';

const wrapWithGestureHandler = (component: FC) => {
  return CodePush(WrappedComponent(gestureHandlerRootHOC(component)));
};

const wrap = (component: FC) => {
  return CodePush(WrappedComponent(component));
};

export const registerScreens = () => {
  Navigation.registerComponent(SCREENS.HOME_SCREEN, () =>
    wrapWithGestureHandler(HomeScreen),
  );

  Navigation.registerComponent(SCREENS.SIGN_IN_SCREEN, () =>
    wrap(SignInScreen),
  );

  Navigation.registerComponent(SCREENS.PROFILE_SCREEN, () =>
    wrap(ProfileScreen),
  );

  Navigation.registerComponent(SCREENS.SPLASH_SCREEN, () => wrap(SplashScreen));

  console.info('All screens have been registered...');
};
