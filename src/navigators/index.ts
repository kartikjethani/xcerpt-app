import {Navigation} from 'react-native-navigation';
import {SCREENS} from '../views/screens';
import {registerScreens} from './register-screens';

registerScreens();

export const pushHomeScreen = () => {
  Navigation.setRoot({
    root: {
      component: {
        name: SCREENS.HOME_SCREEN,
      },
    },
  });
};

export const pushSplashScreen = () => {
  Navigation.setRoot({
    root: {
      component: {
        name: SCREENS.SPLASH_SCREEN,
      },
    },
  });
};
