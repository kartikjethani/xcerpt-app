export const COLORS = {
  white: '#ffffff',
  green: '#2fa84f',
  ratingGreen: '#2fa84f',
  ratingRed: '#ea3d2f',
  ratingYellow: '#f3a72e',
};

export const GRADIENT_COLORS = {
  blue: '#144aa2',
  white: '#ffffff',
  gray: '#dddddd',
  black: '#000000',
  transparent: 'transparent',
};
