export interface AuthToken {
  userName: string;
  idToken: string;
  refreshToken: string;
}
