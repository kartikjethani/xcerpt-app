import React, {FC, memo} from 'react';
import {Text} from 'native-base';
import {tailwind} from 'tailwind';
import {StyleProp, TextStyle} from 'react-native';

interface Props {
  size?: 'xs' | 'sm' | 'lg' | 'xl' | '2xl' | '3xl' | '4xl';
  style?: StyleProp<TextStyle>;
}

export const CText: FC<Props> = memo(({size = 'sm', style, children}) => (
  <Text style={[tailwind(`font-nunito text-${size} text-gray-400`), style]}>
    {children}
  </Text>
));
