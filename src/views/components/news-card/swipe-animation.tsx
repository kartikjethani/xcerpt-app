import React, {FC, memo} from 'react';
import LottieView from 'lottie-react-native';
import SWIPE_UP from '../../../animations/swipe-up.json';
import {View} from 'react-native';
import {CText} from '../shared';
import {tailwind} from 'tailwind';

export const SwipeAnimation: FC = memo(() => (
  <View style={tailwind('absolute bottom-0 flex-1 h-24 w-full')}>
    <LottieView source={SWIPE_UP} autoPlay />
    <CText style={tailwind('flex-row self-center absolute bottom-4')}>
      swipe up
    </CText>
  </View>
));
