import React, {FC, memo} from 'react';
import {Button, View} from 'native-base';
import {tailwind} from 'tailwind';
import LinearGradient from 'react-native-linear-gradient';
import {CText} from '../shared';
import {GRADIENT_COLORS} from '../../../styles/colors';
import {SAVE_USER_CURATION_RESPONSE} from '../../screens/home/home.gql';
import {useMutation} from '@apollo/client';

const blackGradient = [GRADIENT_COLORS.gray, GRADIENT_COLORS.black];

interface Props {
  userId: string;
  userArticleCurationId: number;
}

export const CurationFooter: FC<Props> = memo(
  ({userId, userArticleCurationId}) => {
    const [saveCurationResponse] = useMutation(SAVE_USER_CURATION_RESPONSE);

    return (
      <View style={tailwind('absolute bottom-0 w-full')}>
        <LinearGradient style={[tailwind('h-20')]} colors={blackGradient}>
          <View
            style={tailwind('flex-row self-center absolute bottom-0 w-4/5')}>
            <Button
              block
              style={tailwind('rounded-b-none w-1/2 bg-white mr-1')}
              onPress={() =>
                saveCurationResponse({
                  variables: {
                    input: {
                      userId,
                      userArticleCurationId,
                      curationResponse: 'YES000',
                    },
                  },
                })
              }>
              <CText size={'xl'} style={tailwind('text-gray-800')}>
                Yes
              </CText>
            </Button>
            <Button
              block
              style={tailwind('rounded-b-none w-1/2 bg-white')}
              onPress={() =>
                saveCurationResponse({
                  variables: {
                    input: {
                      userId,
                      userArticleCurationId,
                      curationResponse: 'NO0000',
                    },
                  },
                })
              }>
              <CText size={'xl'} style={tailwind('text-gray-800')}>
                No
              </CText>
            </Button>
          </View>
        </LinearGradient>
      </View>
    );
  },
);
