import React, {FC, memo} from 'react';
import {
  Image,
  StyleSheet,
  FlatList,
  Dimensions,
  // useWindowDimensions,
} from 'react-native';
import {Card, CardItem, Body, View, H3} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
// import Carousel, {Pagination} from 'react-native-snap-carousel';
import {StarIcon} from '../icons';
import {CText} from '../shared';
import {tailwind} from 'tailwind';
import {CurationFooter} from './curation-footer';
import {COLORS, GRADIENT_COLORS} from '../../../styles/colors';
import {SwipeAnimation} from './swipe-animation';
import {globalConfig} from '../../../config';

const screenWidth = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  paginator: {
    width: 40,
    height: 5,
    borderRadius: 5,
    marginHorizontal: 8,
    backgroundColor: 'rgba(255, 255, 255, 0.92)',
  },
  buttonText: {
    color: COLORS.white,
    ...tailwind('font-nunito font-semibold text-lg left-28'),
  },
  heading: {
    color: COLORS.white,
    width: screenWidth - 10,
    ...tailwind('font-nunito mx-1 h-96 px-3 leading-7 h-28'),
  },
  ratingText: {
    color: COLORS.white,
    ...tailwind('font-nunito font-semibold text-lg'),
  },
  headingCarousel: {
    width: '88%',
  },
  ratingButtonGreen: {
    backgroundColor: COLORS.ratingGreen,
    right: '100%',
    ...tailwind('flex-row rounded-xl w-3/12 items-center'),
  },
  ratingButtonYellow: {
    backgroundColor: COLORS.ratingYellow,
    right: '100%',
    ...tailwind('flex-row rounded-xl w-3/12 items-center'),
  },
  ratingButtonRed: {
    backgroundColor: COLORS.ratingRed,
    right: '100%',
    ...tailwind('flex-row rounded-xl w-3/12 items-center'),
  },
});

const getRatingStyle = (
  rating: number,
  _config = {green: 3.5, yellow: 2.5},
) => {
  let style = styles.ratingButtonRed;
  if (rating >= _config.green) {
    style = styles.ratingButtonGreen;
  } else if (rating >= _config.yellow) {
    style = styles.ratingButtonYellow;
  }
  return style;
};

export interface NewsCardProps {
  id: number;
  userId: string;
  image: string;
  headings: Array<string>;
  viewMode?: 'default' | 'curate';
  rating: number;
  cursor: string;
  showSwipeAnimation?: boolean;
}

const renderCarouselItem = ({item}: {item: string}) => (
  <H3 style={styles.heading}>{item}</H3>
);

// interface PaginationProps {
//   activeSlide: number;
//   length: number;
// }

// const CarouselPagination = memo(({activeSlide, length}: PaginationProps) => {
//   return (
//     <Pagination
//       key={activeSlide}
//       dotsLength={length}
//       activeDotIndex={activeSlide}
//       dotStyle={styles.paginator}
//       inactiveDotOpacity={0.4}
//       inactiveDotScale={0.6}
//     />
//   );
// });

export const NewsCard: FC<NewsCardProps> = memo(
  ({
    id,
    userId,
    image,
    headings,
    viewMode = 'default',
    rating,
    showSwipeAnimation = true,
  }) => {
    // const {width: screenwidth} = useWindowDimensions();
    // const [activeSlide, setActiveSlide] = useState(0);

    const {blue, white, transparent, black} = GRADIENT_COLORS;
    const blueGradient = ['#0070ba', '#1546a0'];
    let preGradient = [transparent, blue];
    let bgGradient = [blue, white];
    let buttonGradient = blueGradient;
    // let buttonBg = COLORS.green;
    let buttonBorder = {};
    let buttonText = '94% users trust this';

    if (viewMode === 'curate') {
      preGradient = [transparent, black];
      bgGradient = [black, white];
      buttonGradient = bgGradient;
      // buttonBg = {backgroundColor: black};
      buttonBorder = {borderColor: 'white', borderWidth: 1};
      buttonText = 'verify to earn 1 token';
    }

    const renderCurationFooter = () => {
      return (
        viewMode === 'curate' && (
          <CurationFooter userId={userId} userArticleCurationId={id} />
        )
      );
    };

    return (
      <>
        <Card style={tailwind('w-full m-0 border-0 flex-grow')} noShadow={true}>
          <CardItem style={tailwind('p-0 h-3/5')}>
            <Image
              source={{
                uri: `${globalConfig.assetsBaseUrl}/${image}`,
                // uri: `https://unsplash.it/${screenwidth}/400?image=${id}`,
              }}
              style={tailwind('w-full h-full')}
            />
            <LinearGradient
              style={tailwind('w-full h-20 absolute bottom-0')}
              colors={preGradient}
            />
          </CardItem>
          <CardItem style={tailwind('p-0 h-2/5')}>
            <Body style={tailwind('m-0 p-0')}>
              <LinearGradient
                style={tailwind('w-full h-full')}
                colors={bgGradient}>
                <View style={tailwind('w-full')}>
                  <FlatList
                    horizontal
                    pagingEnabled={true}
                    legacyImplementation={false}
                    data={headings}
                    renderItem={renderCarouselItem}
                    keyExtractor={item => item}
                  />
                  {/* <Carousel
                    useScrollView={true}
                    autoplay
                    loop
                    autoplayInterval={5000}
                    data={headings}
                    onSnapToItem={setActiveSlide}
                    renderItem={renderCarouselItem}
                    sliderWidth={screenwidth}
                    itemWidth={screenwidth}
                    sliderHeight={100}
                  /> */}
                </View>
                {/* <View style={tailwind('self-center absolute top-32')}>
                  <CarouselPagination
                    activeSlide={activeSlide}
                    length={headings.length}
                  />
                </View> */}
                <View
                  style={tailwind(
                    'h-12 w-full flex-row absolute top-48 self-center px-6',
                  )}>
                  <View
                    style={[
                      buttonBorder,
                      tailwind('flex-row rounded-xl w-full'),
                    ]}>
                    <LinearGradient
                      style={tailwind(
                        'flex-row rounded-xl w-full items-center',
                      )}
                      colors={buttonGradient}>
                      <CText style={styles.buttonText}>{buttonText}</CText>
                    </LinearGradient>
                    <View style={getRatingStyle(rating)}>
                      <StarIcon />
                      <CText style={styles.ratingText}>
                        {viewMode === 'curate' ? '--' : rating}
                      </CText>
                    </View>
                  </View>
                </View>
                {renderCurationFooter()}
              </LinearGradient>
            </Body>
          </CardItem>
        </Card>
        {showSwipeAnimation && <SwipeAnimation />}
      </>
    );
  },
);
