import React, {FC, memo} from 'react';
import {Image} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  H3,
  Body,
  Icon,
  View,
} from 'native-base';
import {tailwind} from 'tailwind';
import {NewsRatings} from '../news-ratings';

export interface NewsCardProps {
  id: number;
  imageUrl: string;
  title: string;
  summary: string;
  subText: string;
  rating: number;
  verifiedBy: string;
}
export const NewsCard: FC<NewsCardProps> = ({
  id,
  imageUrl,
  title,
  summary,
  subText,
  rating,
  verifiedBy,
}) => {
  return (
    <Container style={[tailwind('flex-1')]} key={id}>
      <Content
        scrollEnabled={false}
        contentContainerStyle={tailwind('flex-grow')}>
        <Card
          style={[tailwind('w-full m-0 border-0 flex-grow')]}
          noShadow={true}>
          <CardItem style={tailwind('p-0 h-72')}>
            <Image
              source={{
                uri: imageUrl,
              }}
              style={tailwind('h-full w-full')}
            />
          </CardItem>
          <CardItem style={tailwind('p-0 flex-grow')}>
            <Body>
              <H3 style={tailwind('p-3 font-nunito leading-7 max-h-28')}>
                {title}
              </H3>
              <Text style={tailwind('text-xs px-3 text-gray-400 mb-3')}>
                {subText}
              </Text>
              <Text
                style={tailwind(
                  'px-3 font-nunito overflow-hidden leading-6 max-h-48',
                )}>
                {summary}
              </Text>
              <Text style={tailwind('text-xs px-3 text-gray-400 my-3')}>
                Swipe right for full story
              </Text>
            </Body>
          </CardItem>
        </Card>
      </Content>
      <Card style={[tailwind('w-full m-0')]}>
        <CardItem style={tailwind('p-0 px-3')}>
          <Body>
            <View style={tailwind('flex-row w-full h-20 items-center')}>
              <NewsRatings rating={rating} />
              <Text style={[tailwind('text-sm ml-2 text-gray-400')]}>
                Verified by {verifiedBy}
              </Text>
              <Icon
                type="FontAwesome5"
                name="info-circle"
                style={tailwind('text-gray-400 text-base ml-2')}
              />
            </View>
          </Body>
        </CardItem>
      </Card>
    </Container>
  );
};

export default memo(NewsCard);
