import React, {FC} from 'react';
import {StarIcon, IconColors} from '../icons';

export enum Ratings {
  One = 1,
  Two = 2,
  Three = 3,
  Four = 4,
  Five = 5,
}

export interface Props {
  rating: Ratings;
}

const ratings = [1, 2, 3, 4, 5];

export const NewsRatings: FC<Props> = ({rating}) => {
  let color: IconColors;
  switch (rating) {
    case 1:
    case 2:
      color = IconColors.red;
      break;
    case 3:
      color = IconColors.yellow;
      break;
    case 4:
    case 5:
      color = IconColors.green;
      break;
    default:
      break;
  }

  return (
    <>
      {ratings.map(r => {
        const solid = r <= rating;
        return <StarIcon solid={solid} color={color} key={r} />;
      })}
    </>
  );
};
