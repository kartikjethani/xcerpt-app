import React, {ReactNode, useEffect, useState} from 'react';
import {useWindowDimensions, ViewProps} from 'react-native';
import {
  PanGestureHandler,
  PanGestureHandlerGestureEvent,
} from 'react-native-gesture-handler';
import Animated, {
  interpolate,
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
  withTiming,
} from 'react-native-reanimated';
import {tailwind} from 'tailwind';
import {CText} from '../shared';

const SWIPE_VELOCITY = 700;

const springConfig = {
  damping: 12,
  tension: 1,
  friction: 2,
  mass: 0.2,
  stiffness: 121.6,
  overshootClamping: false,
  restSpeedThreshold: 0.001,
  restDisplacementThreshold: 0.001,
  toValue: 0,
};

export type StackProps<T> = Pick<ViewProps, 'style'> & {
  data: T[];
  renderItem: (item: T, index: number) => ReactNode;
  onSwipeUp?: (item: T) => void;
  onSwipeDown?: (item: T) => void;
};

const NewsDeckSwiper = <T,>({
  renderItem,
  data,
  onSwipeUp,
  onSwipeDown,
}: StackProps<T>) => {
  const {height: screenHeight} = useWindowDimensions();
  const hiddenTranslateY = screenHeight;

  const [currentIndex, setCurrentIndex] = useState(0);
  const nextIndex = currentIndex + 1;
  const prevIndex = currentIndex - 1;

  const currentItem = data[currentIndex];
  const nextItem = data[nextIndex];
  const prevItem = data[prevIndex];

  const previousY = useSharedValue(-hiddenTranslateY);
  const translateY = useSharedValue(0);
  const scrollDirection = useSharedValue('');

  const setIndex = (index: number) => {
    const addend = scrollDirection.value === 'up' ? 1 : -1;
    const _currentIndex = index + addend;
    setCurrentIndex(_currentIndex);
  };

  const gestureHandler = useAnimatedGestureHandler<
    PanGestureHandlerGestureEvent,
    {startY: number}
  >({
    onStart: (_, context) => {
      context.startY = translateY.value;
    },
    onActive: (event, context) => {
      const _direction = context.startY > event.translationY ? 'up' : 'down';
      scrollDirection.value = _direction;

      if (scrollDirection.value === 'up') {
        translateY.value = context.startY + event.translationY;
      } else if (scrollDirection.value === 'down') {
        previousY.value = -hiddenTranslateY + event.translationY;
      }
    },
    onEnd: ({velocityY}) => {
      if (
        Math.abs(velocityY) < SWIPE_VELOCITY ||
        (scrollDirection.value === 'up' && !nextItem) ||
        (scrollDirection.value === 'down' && !prevItem)
      ) {
        translateY.value = withSpring(0, springConfig);
        return;
      }

      if (scrollDirection.value === 'down') {
        previousY.value = withTiming(
          0,
          {
            duration: 250,
          },
          () => runOnJS(setIndex)(currentIndex),
        );
      } else if (scrollDirection.value === 'up') {
        translateY.value = withTiming(
          Math.sign(velocityY) * hiddenTranslateY,
          {
            duration: 250,
          },
          () => runOnJS(setIndex)(currentIndex),
        );
      }

      const onSwipe = scrollDirection.value === 'up' ? onSwipeUp : onSwipeDown;
      onSwipe && runOnJS(onSwipe)(currentItem);
    },
  });

  const prevItemAnimatedStyle = useAnimatedStyle(() => ({
    backgroundColor: 'white',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 2,
    transform: [{translateY: previousY.value}],
  }));

  const currentItemAnimatedStyle = useAnimatedStyle(() => ({
    backgroundColor: 'white',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 2,
    transform: [{translateY: translateY.value}],
  }));

  const nextItemAnimatedStyle = useAnimatedStyle(() => ({
    backgroundColor: 'white',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 1,
    transform: [
      {
        scale: interpolate(
          translateY.value,
          [-hiddenTranslateY, 0, hiddenTranslateY],
          [1, 0.8, 1],
        ),
      },
    ],
    opacity: interpolate(
      translateY.value,
      [-hiddenTranslateY, 0, hiddenTranslateY],
      [1, 0.6, 1],
    ),
  }));

  useEffect(() => {
    previousY.value = -hiddenTranslateY;
    translateY.value = 0;
  }, [currentIndex, previousY, translateY, hiddenTranslateY, scrollDirection]);

  return (
    <>
      <PanGestureHandler key={currentIndex} onGestureEvent={gestureHandler}>
        <Animated.View style={currentItemAnimatedStyle}>
          {renderItem(currentItem, currentIndex)}
        </Animated.View>
      </PanGestureHandler>
      {prevItem && (
        <Animated.View key={prevIndex} style={prevItemAnimatedStyle}>
          {renderItem(prevItem, prevIndex)}
        </Animated.View>
      )}
      {nextItem ? (
        <Animated.View key={nextIndex} style={nextItemAnimatedStyle}>
          {renderItem(nextItem, nextIndex)}
        </Animated.View>
      ) : (
        <CText style={tailwind('self-center absolute bottom-20')}>
          No more items
        </CText>
      )}
    </>
  );
};
export {NewsDeckSwiper};
