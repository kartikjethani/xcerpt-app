import React, {FC, memo} from 'react';
import {Icon} from 'native-base';
import {StyleSheet, ViewStyle} from 'react-native';
import {COLORS} from '../../../styles/colors';
import {tailwind} from 'tailwind';

export enum IconColors {
  green = 'green',
  red = 'red',
  yellow = 'orange',
  white = 'white',
}

interface Props {
  color?: IconColors;
  solid?: boolean;
  style?: ViewStyle;
}

const styles = StyleSheet.create({
  icon: {
    ...tailwind('font-nunito text-lg ml-5 mr-1'),
    color: COLORS.white,
  },
});

export const StarIcon: FC<Props> = memo(() => {
  return (
    <Icon type="FontAwesome5" name="star" solid={true} style={styles.icon} />
  );
});
