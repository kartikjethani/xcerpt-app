import React, {FC} from 'react';
import {View} from 'native-base';
import {ProfileHeader} from './profile-header';

export const ProfileScreen: FC = () => {
  return (
    <View>
      <ProfileHeader name={'Meghna'} balance={272.3} />
    </View>
  );
};
