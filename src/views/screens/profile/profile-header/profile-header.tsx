import {Thumbnail, View} from 'native-base';
import React, {FC} from 'react';
import {Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {tailwind} from 'tailwind';
import BG_LOGO_SHAPE from '../../../../assets/img/bg-logo-shape/bg-logo-shape.png';
import LOGO from '../../../../assets/img/logo/transparent-logo.png';
import {CText} from '../../../components/shared';

const textWhite = {color: 'white'};

interface ProfileHeaderProps {
  name: string;
  balance: number;
}
export const ProfileHeader: FC<ProfileHeaderProps> = ({name, balance}) => (
  <View>
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={['#0070ba', '#1546a0']}
      style={[tailwind('h-64')]}>
      <Image style={tailwind('opacity-20 h-full')} source={BG_LOGO_SHAPE} />
      <Thumbnail
        square
        source={LOGO}
        style={[tailwind('absolute top-12 left-5')]}
      />
      <Thumbnail
        style={[tailwind('absolute top-12 right-5')]}
        source={{uri: 'https://www.w3schools.com/w3images/avatar2.png'}}
      />
      <CText
        size={'lg'}
        style={[tailwind('absolute top-28 left-6 opacity-70'), textWhite]}>
        Hello, {name}!
      </CText>
      <View style={tailwind('bottom-20 px-5')}>
        <CText size={'3xl'} style={[tailwind('p-0'), textWhite]}>
          {balance}
        </CText>
        <CText size={'sm'} style={[tailwind('p-0'), textWhite]}>
          Token balance
        </CText>
      </View>
    </LinearGradient>
  </View>
);
