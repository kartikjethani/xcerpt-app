import {gql} from '@apollo/client';

const CREATE_GUEST_USER = gql`
  mutation {
    createUser(createUserInput: {}) {
      id
    }
  }
`;

export {CREATE_GUEST_USER};
