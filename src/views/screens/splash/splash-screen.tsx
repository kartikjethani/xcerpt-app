import React, {useState, FC, useCallback, useEffect} from 'react';
import {useMutation} from '@apollo/client';
import {CREATE_GUEST_USER} from './splash.gql';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../state/store/store';
import {setUserIdThunk, User} from '../../../state/ducks/users/thunks';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {pushHomeScreen} from '../../../navigators';
import {AppLogo} from './app-logo';

export const SplashScreen: FC = () => {
  const [isLoading, setLoading] = useState(true);
  const [createGuestUser, {data}] = useMutation(CREATE_GUEST_USER);
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.users);

  const fetchUserData = useCallback(async () => {
    const jsonUser = await AsyncStorage.getItem('@user');
    if (jsonUser !== null) {
      const parsedUser: User = JSON.parse(jsonUser);
      dispatch(setUserIdThunk(parsedUser));
      setLoading(false);
    } else {
      createGuestUser();
    }
  }, [createGuestUser, dispatch]);

  const saveUserData = useCallback(
    async (newUser: User) => {
      const {userId} = newUser;
      dispatch(setUserIdThunk({userId}));
      const jsonUser = JSON.stringify(newUser);
      await AsyncStorage.setItem('@user', jsonUser);
      setLoading(false);
    },
    [dispatch],
  );

  useEffect(() => {
    if (user.userId === null) {
      fetchUserData();
    }
  }, [user, fetchUserData]);

  useEffect(() => {
    if (data) {
      const {
        createUser: {id},
      } = data;
      const newUser: User = {
        userId: id,
      };
      saveUserData(newUser);
    }
  }, [data, saveUserData]);

  useEffect(() => {
    if (!isLoading) {
      pushHomeScreen();
    }
  }, [isLoading]);

  return <AppLogo />;
};
