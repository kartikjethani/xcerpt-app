import React, {FC} from 'react';
import {Image, View} from 'react-native';
import {tailwind} from 'tailwind';
import APP_LOGO from '../../../assets/img/logo/logo.png';

export const AppLogo: FC = () => (
  <View style={tailwind('flex-1 flex-row self-center items-center')}>
    <Image style={tailwind('h-32 w-32 rounded-2xl')} source={APP_LOGO} />
  </View>
);
