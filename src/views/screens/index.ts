export {HomeScreen} from './home';
export {ProfileScreen} from './profile';

export const SCREENS = {
  HOME_SCREEN: 'HOME_SCREEN',
  SIGN_IN_SCREEN: 'SIGN_IN_SCREEN',
  PROFILE_SCREEN: 'PROFILE_SCREEN',
  SPLASH_SCREEN: 'SPLASH_SCREEN',
};
