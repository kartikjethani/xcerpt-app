import React, {FC, memo, useCallback, useEffect, useRef} from 'react';
import {NewsCard, NewsCardProps} from '../../components/news-card';
import {NewsDeckSwiper} from '../../components/news-deck-swiper/';
import {useLazyQuery} from '@apollo/client';
import {GET_FEED} from './home.gql';
import {
  getUserPreferences,
  saveUserPreferences,
  UserPreferences,
} from './home.util';
import {useSelector} from 'react-redux';
import {RootState} from '../../../state/store/store';

export const HomeScreen: FC = memo(() => {
  const [loadFeed, {data, loading, fetchMore, error, refetch}] = useLazyQuery(
    GET_FEED,
    {nextFetchPolicy: 'cache-first'},
  );
  const {userId} = useSelector((state: RootState) => state.users);
  const retryCount = useRef(0);

  if (error && retryCount.current < 3) {
    retryCount.current++;
    setTimeout(() => {
      refetch && refetch();
    }, 300);
  }

  useEffect(() => {
    (async () => {
      const {
        lastViewedArticle: {cursor},
      } = await getUserPreferences();
      loadFeed({
        variables: {
          userId,
          first: 100,
          after: cursor,
        },
      });
    })();
  }, [loadFeed, userId]);

  const renderItem = useCallback(
    (card: NewsCardProps) => <NewsCard {...card} />,
    [],
  );

  if (loading || !data) {
    return null;
  }

  const {
    feed: {
      edges,
      pageInfo: {endCursor},
      curation,
    },
  } = data;

  const loadMore = () => {
    // @ts-ignore
    fetchMore({
      variables: {
        userId,
        first: 100,
        after: endCursor,
      },
    });
  };

  const onSwipeUp = (item: NewsCardProps) => {
    //update user preferences
    const {id, cursor} = item;
    const preferences: UserPreferences = {
      lastViewedArticle: {id, cursor},
      showSwipeAnimation: false,
    };
    saveUserPreferences(preferences);
    // load more articles
    if (id % 50 === 0) {
      loadMore();
    }
  };

  const newsFeed: NewsCardProps[] = [];

  if (curation && curation.id) {
    newsFeed.push({...curation, rating: 0, userId, viewMode: 'curate'});
  }

  const _edges = JSON.parse(JSON.stringify(edges));
  _edges.map(({node, cursor}: any) => {
    node.cursor = cursor;
    newsFeed.push({
      ...node,
      userId,
      viewMode: 'default',
    });
  });

  return (
    <NewsDeckSwiper
      data={newsFeed}
      renderItem={renderItem}
      onSwipeUp={onSwipeUp}
    />
  );
});
