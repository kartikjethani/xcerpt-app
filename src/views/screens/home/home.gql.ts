import {gql} from '@apollo/client';

const GET_FEED = gql`
  query GET_FEED($userId: String!, $first: Int!, $after: String) {
    feed(userId: $userId, first: $first, after: $after) {
      edges {
        cursor
        node {
          id
          headings
          image
          sourceUrl
          rating
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
      curation {
        id
        headings
        image
        curationQuestion
      }
    }
  }
`;

const SAVE_USER_CURATION_RESPONSE = gql`
  mutation SAVE_USER_CURATION_RESPONSE($input: UserCurationResponseInput!) {
    saveUserCurationResponse(input: $input) {
      userResponse
    }
  }
`;

export {GET_FEED, SAVE_USER_CURATION_RESPONSE};
