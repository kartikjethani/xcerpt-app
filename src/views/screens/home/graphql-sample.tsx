import React, {FC} from 'react';
import {View, Text, Button, ActivityIndicator} from 'react-native';
import {userSelectors} from '../../../state/ducks/users';
import {useDispatch} from 'react-redux';
import {setLoggedInThunk} from '../../../state/ducks/users/thunks';
import {gql, useQuery} from '@apollo/client';

const USERS_QUERY = gql`
  query {
    users {
      firstName
      lastName
    }
  }
`;

const Loading: FC = () => (
  <View style={{padding: 100}}>
    <ActivityIndicator size="large" color={'#ff5dc8'} />
  </View>
);

const HomeScreen = () => {
  const {
    data: {users},
    loading,
  } = useQuery(USERS_QUERY);

  const _isLoggedIn = userSelectors.IsLoggedIn();
  const dispatch = useDispatch();

  if (loading) {
    return <Loading />;
  }

  return (
    <View style={{padding: 100}}>
      {users.map((user: any) => (
        <Text key={user.firstName}>
          {user.firstName} {user.lastName}
        </Text>
      ))}
      <Text>{_isLoggedIn.toString()}</Text>
      <Button
        title="Push Settings Screen"
        color="#710ce3"
        onPress={() =>
          dispatch(setLoggedInThunk({isLoggedIn: !_isLoggedIn, userId: ''}))
        }
      />
    </View>
  );
};

HomeScreen.options = {
  topBar: {
    title: {
      text: 'Home',
    },
  },
  bottomTab: {
    text: 'Home',
  },
};

export default HomeScreen;
