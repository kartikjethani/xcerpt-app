import AsyncStorage from '@react-native-async-storage/async-storage';

export interface UserPreferences {
  lastViewedArticle: {
    id: number | null;
    cursor: string | null;
  };
  showSwipeAnimation: boolean;
}

const initialPreferences: UserPreferences = {
  lastViewedArticle: {id: null, cursor: null},
  showSwipeAnimation: true,
};

export const getUserPreferences = async () => {
  const jsonPreferences = await AsyncStorage.getItem('@userPreferences');
  let preferences: UserPreferences = initialPreferences;
  if (jsonPreferences) {
    preferences = JSON.parse(jsonPreferences);
  }
  return preferences;
};

export const saveUserPreferences = (preferences: UserPreferences) => {
  const jsonPreferences = JSON.stringify(preferences);
  AsyncStorage.setItem('@userPreferences', jsonPreferences);
};
