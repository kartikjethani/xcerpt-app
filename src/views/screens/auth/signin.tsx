import React, {FC, useCallback} from 'react';
import {NewsCard, NewsCardProps} from '../../components/news-card';
import {NewsDeckSwiper} from '../../components/news-deck-swiper';

const cards: NewsCardProps[] = [
  {
    id: 1,
    imageUrl:
      'https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/joe_hero_660_240421075800.jpg',
    title:
      'Pressure on Joe Biden administration to ship AstraZeneca vaccine, supplies to India',
    summary:
      'The Biden administration has came under intense pressure from various quarters, including the powerful US Chambers of Commerce, lawmakers and eminent Indian-Americans, to ship AstraZeneca and other Coronavirus (Covid-19) vaccines along with several life-saving medical supplies to India, which is witnessing a deadly surge in coronavirus cases.',
    subText: '5m. California, USA',
    rating: 3,
    verifiedBy: '14.6K',
  },
  {
    id: 2,
    imageUrl:
      'https://c.ndtvimg.com/2021-05/kh6394to_cyclonetauktae650afp_625x300_16_May_21.jpg',
    title: 'Thousands Moved As Rain From Cyclone Tauktae Hammers West Coast',
    summary:
      'Severe cyclonic storm Tauktae intensified overnight into a "very severe cyclonic storm" over the east-central Arabian Sea, the weather office said this morning. It is very likely to intensify further and reach the Gujarat coast in the evening of May 17. It is expected to make landfall the next morning between Porbandar and Mahuva in Bhavnagar district, it added.',
    subText: '5m. California, USA',
    rating: 2,
    verifiedBy: '12.1K',
  },
  {
    id: 3,
    imageUrl:
      'https://c.ndtvimg.com/2021-05/5t79ht94_isralegazarocketsafp650_625x300_16_May_21.jpg',
    title: 'Highest Ever Rate Of Rocket Attacks On Israel This Time, Says Army',
    summary:
      "Jerusalem: Israel has faced the highest ever rate of rocket attacks on its territory during its latest confrontation with the Palestinian Islamist group Hamas that controls Gaza, the army said Sunday. Since Monday, armed groups in Gaza have fired about 3,000 rockets towards Israel, surpassing the pace during an escalation in 2019 and during the 2006 war with Lebanon's Hezbollah.",
    subText: '5m. California, USA',
    rating: 4,
    verifiedBy: '14.6K',
  },
  {
    id: 4,
    imageUrl:
      'https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/joe_hero_660_240421075800.jpg',
    title:
      'Pressure on Joe Biden administration to ship AstraZeneca vaccine, supplies to India',
    summary:
      'The Biden administration has came under intense pressure from various quarters, including the powerful US Chambers of Commerce, lawmakers and eminent Indian-Americans, to ship AstraZeneca and other Coronavirus (Covid-19) vaccines along with several life-saving medical supplies to India, which is witnessing a deadly surge in coronavirus cases.',
    subText: '5m. California, USA',
    rating: 3,
    verifiedBy: '14.6K',
  },
  {
    id: 5,
    imageUrl:
      'https://c.ndtvimg.com/2021-05/kh6394to_cyclonetauktae650afp_625x300_16_May_21.jpg',
    title: 'Thousands Moved As Rain From Cyclone Tauktae Hammers West Coast',
    summary:
      'Severe cyclonic storm Tauktae intensified overnight into a "very severe cyclonic storm" over the east-central Arabian Sea, the weather office said this morning. It is very likely to intensify further and reach the Gujarat coast in the evening of May 17. It is expected to make landfall the next morning between Porbandar and Mahuva in Bhavnagar district, it added.',
    subText: '5m. California, USA',
    rating: 2,
    verifiedBy: '12.1K',
  },
  {
    id: 6,
    imageUrl:
      'https://c.ndtvimg.com/2021-05/5t79ht94_isralegazarocketsafp650_625x300_16_May_21.jpg',
    title: 'Highest Ever Rate Of Rocket Attacks On Israel This Time, Says Army',
    summary:
      "Jerusalem: Israel has faced the highest ever rate of rocket attacks on its territory during its latest confrontation with the Palestinian Islamist group Hamas that controls Gaza, the army said Sunday. Since Monday, armed groups in Gaza have fired about 3,000 rockets towards Israel, surpassing the pace during an escalation in 2019 and during the 2006 war with Lebanon's Hezbollah.",
    subText: '5m. California, USA',
    rating: 4,
    verifiedBy: '14.6K',
  },
  {
    id: 7,
    imageUrl:
      'https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/joe_hero_660_240421075800.jpg',
    title:
      'Pressure on Joe Biden administration to ship AstraZeneca vaccine, supplies to India',
    summary:
      'The Biden administration has came under intense pressure from various quarters, including the powerful US Chambers of Commerce, lawmakers and eminent Indian-Americans, to ship AstraZeneca and other Coronavirus (Covid-19) vaccines along with several life-saving medical supplies to India, which is witnessing a deadly surge in coronavirus cases.',
    subText: '5m. California, USA',
    rating: 3,
    verifiedBy: '14.6K',
  },
  {
    id: 8,
    imageUrl:
      'https://c.ndtvimg.com/2021-05/kh6394to_cyclonetauktae650afp_625x300_16_May_21.jpg',
    title: 'Thousands Moved As Rain From Cyclone Tauktae Hammers West Coast',
    summary:
      'Severe cyclonic storm Tauktae intensified overnight into a "very severe cyclonic storm" over the east-central Arabian Sea, the weather office said this morning. It is very likely to intensify further and reach the Gujarat coast in the evening of May 17. It is expected to make landfall the next morning between Porbandar and Mahuva in Bhavnagar district, it added.',
    subText: '5m. California, USA',
    rating: 2,
    verifiedBy: '12.1K',
  },
  {
    id: 9,
    imageUrl:
      'https://c.ndtvimg.com/2021-05/5t79ht94_isralegazarocketsafp650_625x300_16_May_21.jpg',
    title: 'Highest Ever Rate Of Rocket Attacks On Israel This Time, Says Army',
    summary:
      "Jerusalem: Israel has faced the highest ever rate of rocket attacks on its territory during its latest confrontation with the Palestinian Islamist group Hamas that controls Gaza, the army said Sunday. Since Monday, armed groups in Gaza have fired about 3,000 rockets towards Israel, surpassing the pace during an escalation in 2019 and during the 2006 war with Lebanon's Hezbollah.",
    subText: '5m. California, USA',
    rating: 4,
    verifiedBy: '14.6K',
  },
];
const CardShowcaseExample: FC = () => {
  const renderItem = useCallback(
    (card: NewsCardProps) => <NewsCard {...card} />,
    [],
  );

  return <NewsDeckSwiper data={cards} renderItem={renderItem} />;
};

export default CardShowcaseExample;
