import './wdyr';
import 'react-native-gesture-handler';
import {Navigation} from 'react-native-navigation';
import {App} from './App';

import * as Sentry from '@sentry/react-native';

if (!__DEV__) {
  Sentry.init({
    dsn: 'https://4425742d97e14cd1be5900ca2f257903@o567157.ingest.sentry.io/5710810', // TODO: Move this to env
  });
}
App();
