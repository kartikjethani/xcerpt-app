import {Navigation} from 'react-native-navigation';
import {pushSplashScreen} from './src/navigators';

export const App = () => {
  Navigation.events().registerAppLaunchedListener(() => pushSplashScreen());
};
